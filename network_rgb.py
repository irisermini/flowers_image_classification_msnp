import numpy as np


def network_learning(class_index, n1, n2, n3, train_r, train_g, train_b, data_truth, test_r, test_g, test_b):    
    network = {}
    
    w1 = np.zeros((1, n2 * n1), dtype=int)
    
    w1[0, 0::2 * n1 + 2] = 1
    w1[0, 1::2 * n1 + 2] = 1
    w1 = np.reshape(w1, (n1, n2)).T
    
    network["W1"] = w1

    assert (np.shape(network['W1']) == (n2, n1))
    
    network["W2"] = np.zeros((n3, n2), dtype=int)

    assert (np.shape(network['W2']) == (n3, n2))
    
    network["W3"] = np.ones((1, n3), dtype=int)

    assert (np.shape(network['W3']) == (1, n3))
    
    train_r = train_r + 1
    train_g = train_g + 1
    train_b = train_b + 1
    
    w3_index = 0
    
    for i in range(0, len(train_r)):
        data_r_i = np.copy(train_r[i])
        data_g_i = np.copy(train_g[i])
        data_b_i = np.copy(train_b[i])
        data_r_i = np.reshape(data_r_i, (-1, 1))
        data_g_i = np.reshape(data_g_i, (-1, 1))
        data_b_i = np.reshape(data_b_i, (-1, 1))
        data_i = np.concatenate((data_r_i,data_g_i,data_b_i), axis=0)
        
        linear_1 = np.dot(network["W1"], data_i)
        activation_1 = np.copy(linear_1)
        
        activation_1[::2] = np.where(activation_1[::2] == 1, 1, 0)
        activation_1[1::2] = np.where(activation_1[::2] == 1, 0, 1)
        
        if data_truth[i] == class_index:
            print(w3_index)
            print(i)
            network["W2"][w3_index % n3] = network["W2"][w3_index % n3] + activation_1.T
            w3_index = w3_index + 1
        
    print("Testing...")    
    
    test_r = test_r + 1
    test_g = test_g + 1
    test_b = test_b + 1
    
    test_r = test_r.T
    test_g = test_g.T
    test_b = test_b.T
    
    data_test = np.concatenate((test_r,test_g,test_b), axis=0)
    activations = {}
    linear_1 = np.dot(network["W1"], data_test)
    linear_1[::2, :] = np.where(linear_1[::2, :] == 1, 1, 0)
    linear_1[1::2, :] = np.where(linear_1[::2, :] == 1, 0, 1)
    activations["activation_1"] = linear_1
    activations["activation_2"] = np.dot(network["W2"], activations["activation_1"])
    activations["activation_3"] = np.dot(network["W3"], activations["activation_2"])
    return network, np.max(activations["activation_2"].T, axis=1).T
    #return network, activations["activation_3"]
    #return network, np.max(activations["activation_3"].T, axis=1).T