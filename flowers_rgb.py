import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np
import cv2
import matplotlib.pyplot as plt
import math
from network_rgb import network_learning

AUTOTUNE = tf.data.experimental.AUTOTUNE
IMG_CP_SIZE = 150
SCALE_FACTOR = 0.06
PIXEL_THRESHOLD = 100


def crop(image, label):
    image = tf.image.resize_with_crop_or_pad(image, IMG_CP_SIZE, IMG_CP_SIZE)
    return image, label


# Tensorflow settings for runtime optimization for CPU
tf.config.threading.set_inter_op_parallelism_threads(4)
tf.config.threading.set_intra_op_parallelism_threads(0)
tf.config.set_soft_device_placement(enabled=True)
 
# Using train ds as test ds and viceversa
#(train_ds, test_ds, val_ds), info_ds = tfds.load(
#    "oxford_flowers102",
#    split=["test", "train", "validation"],
#    shuffle_files=True,
#    as_supervised=True,
#    with_info=True
#)
(test_ds, train_ds, val_ds), info_ds = tfds.load(
    "oxford_flowers102",
    split=["test", "train", "validation"],
    shuffle_files=True,
    as_supervised=True,
    with_info=True
)

#Visualize image resolutions
#hs = []
#ws = []
#for (image, label) in train_ds:
#    size = image.numpy().shape[:-1]
#    height = size[0]
#    width = size[1]
#    hs.append(height)
#    ws.append(width)
#fig = plt.figure(figsize=(8, 8))
#ax = fig.add_subplot(111)
#points = ax.scatter(ws, hs, color='blue', alpha=0.5)
#ax.set_title("Image Resolution")
#ax.set_xlabel("Width", size=14)
#ax.set_ylabel("Height", size=14)


train_ds = train_ds.map(crop, num_parallel_calls=AUTOTUNE)
test_ds = test_ds.map(crop, num_parallel_calls=AUTOTUNE)

img_size = int(IMG_CP_SIZE * SCALE_FACTOR)       

# Downscale images   
images = []
labels = []
for image, label in train_ds:
    img_res = cv2.resize(image.numpy(), (0, 0), fx = SCALE_FACTOR, fy = SCALE_FACTOR, interpolation=cv2.INTER_AREA)      
    images.append(img_res)
    labels.append(label)
    
train_x = np.stack(images, axis=0)

# Pixel RGB values: from {0,1,...,255} to {0,1}
train_x = np.where(train_x >= PIXEL_THRESHOLD, 1, 0)
    
train_x_r = train_x[...,0]
train_x_g = train_x[...,1]
train_x_b = train_x[...,2]
train_x_r = np.reshape(train_x_r, (tf.data.experimental.cardinality(train_ds), img_size*img_size))
train_x_g = np.reshape(train_x_g, (tf.data.experimental.cardinality(train_ds), img_size*img_size))
train_x_b = np.reshape(train_x_b, (tf.data.experimental.cardinality(train_ds), img_size*img_size))

train_y = np.stack(labels, axis=0)

images = []
labels = []
for image, label in test_ds: 
    img_res = cv2.resize(image.numpy(), (0, 0), fx = SCALE_FACTOR, fy = SCALE_FACTOR, interpolation=cv2.INTER_AREA)       
    images.append(img_res)
    labels.append(label)

test_x = np.stack(images, axis=0)

# Pixel RGB values: from {0,1,...,255} to {0,1}
test_x_bin = np.where(test_x >= PIXEL_THRESHOLD, 1, 0)

test_x_r = test_x_bin[...,0]
test_x_g = test_x_bin[...,1]
test_x_b = test_x_bin[...,2]  
test_x_r = np.reshape(test_x_r, (tf.data.experimental.cardinality(test_ds), img_size*img_size))
test_x_g = np.reshape(test_x_g, (tf.data.experimental.cardinality(test_ds), img_size*img_size))
test_x_b = np.reshape(test_x_b, (tf.data.experimental.cardinality(test_ds), img_size*img_size))

test_y = np.stack(labels, axis=0)

num_classes = info_ds.features['label'].num_classes

names_classes = info_ds.features['label'].names

    
# Flower class to be learned
class_index = 0
flower_class = names_classes[class_index]
    
# Using num of images of class to be learned as n3
num_img = 0
for image, label in train_ds:
    if label == class_index:
        num_img += 1
n3 = num_img

print("Training: ")  
for image, label in train_ds:
    if label == class_index:
        plt.imshow(image.numpy())
        plt.show()
        
print("Test: ")  
for image, label in test_ds:
    if label == class_index:
        plt.imshow(image.numpy())
        plt.show()
     
print(f'\nLearning {flower_class}, class index {class_index}') 
    
network, predictions = network_learning(class_index, img_size*img_size*3, img_size*img_size*3*2, n3,
                                        train_x_r, train_x_g, train_x_b, train_y,
                                        test_x_r, test_x_g, test_x_b)
    
    
predictions = predictions / num_img
    
import pickle
with open("zero.net", "wb") as f:
    pickle.dump(network, f)
import re
with open("max_" + re.sub("\W", "", flower_class), "wb") as f:
    pickle.dump(predictions, f)
    
#import pickle
#with open("zero.net", "rb") as f:
#    network = pickle.load(f)
#import re
#with open("max_" + re.sub("\W", "", flower_class), "rb") as f:
#    predictions = pickle.load(f) 
    
min_preds = np.min(predictions)
max_preds = np.max(predictions)
    
output_test = np.where(test_y == class_index, 1, 0)
    
num_test_images = len(output_test)
num_pos = np.count_nonzero(output_test == 1)
num_neg = np.count_nonzero(output_test == 0)
    
# Calculate weights
w_tn = num_test_images / (2 * num_neg)
w_tp = (num_neg / num_pos) * w_tn
    
max_w_acc = 0
best_dt = 0
    
for dt in range(math.ceil(min_preds), math.floor(max_preds)):
    # Num of spikes less/more than threshold -> 0(negative)/1(positive)
    predictions_bool = np.where(predictions.T > dt, 1, 0)
    tp = np.sum((predictions_bool == output_test) * (predictions_bool == 1))
    tn = np.sum((predictions_bool == output_test) * (predictions_bool == 0))
    fp = np.sum((predictions_bool != output_test) * (predictions_bool == 1))
    fn = np.sum((predictions_bool != output_test) * (predictions_bool == 0))
    w_acc = (w_tp * tp + w_tn * tn) / (tp + tn + fp + fn)
    if w_acc > max_w_acc:
        max_w_acc = w_acc
        best_dt = dt
    
print("Best dt: ", best_dt)
    
predictions_bool = np.where(predictions.T > best_dt, 1, 0)
tp = np.sum((predictions_bool == output_test) * (predictions_bool == 1))
tn = np.sum((predictions_bool == output_test) * (predictions_bool == 0))
fp = np.sum((predictions_bool != output_test) * (predictions_bool == 1))
fn = np.sum((predictions_bool != output_test) * (predictions_bool == 0))
print("tp: ", tp)
print("tn: ", tn)
print("fp: ", fp) 
print("fn: ", fn)
    
print("accuracy: ", (tp + tn) / (tp + tn + fp + fn))
print("weighted accuracy: ", (w_tp * tp + w_tn * tn) / (tp + tn + fp + fn))